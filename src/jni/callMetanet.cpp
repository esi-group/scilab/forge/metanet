/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "Metanet.hxx"
#include "callMetanet.hxx"

extern "C" {
#include "Scierror.h"
#include "getScilabJavaVM.h"
}

/*--------------------------------------------------------------------------*/
using namespace org_scilab_modules_metanet;
/*--------------------------------------------------------------------------*/
int callMetanet(char const * variable, char const * file,
                      double window,         double scale,
              double const * winsize,           int winsizeSize)
{
    /* Call the java implementation */
    try
    {
        Metanet::metanet(getScilabJavaVM(), variable, file,
                         window,            scale,    winsize,
                         winsizeSize);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "edit_graph",
                 exception.getJavaDescription().c_str());
        return 1;
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "edit_graph",
                 exception.whatStr().c_str());
        return 1;
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
double callNetwindow(double index)
{
    /* Call the java implementation */
    try
    {
        return Metanet::netwindow(getScilabJavaVM(), index);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "netwindow",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "netwindow",
                 exception.whatStr().c_str());
    }
    return 0;
}
/*--------------------------------------------------------------------------*/
double * callNetwindows(int * len)
{
    /* Call the java implementation */
    try
    {
        return Metanet::netwindows(getScilabJavaVM(), len);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "netwindows",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "netwindows",
                 exception.whatStr().c_str());
    }

    *len = 0;
    return NULL;
}
/*--------------------------------------------------------------------------*/
void callMetanetShow(
                  bool nodes,                 bool edges,
        double const * indexes,                int indexesSize,
                  bool clearPrevious, char const * message)
{
    /* Call the java implementation */
    try
    {
        Metanet::show(getScilabJavaVM(), nodes,       edges,
                      indexes,           indexesSize, clearPrevious,
                      message);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "metanet_show",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "metanet_show",
                 exception.whatStr().c_str());
    }
}
/*--------------------------------------------------------------------------*/
void callWarnObjectByUID(char const * UID, char const * message)
{
    /* Call the java implementation */
    try
    {
        Metanet::warnObjectByUID(getScilabJavaVM(), UID, message);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "warn_object_by_uid",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "warn_object_by_uid",
                 exception.whatStr().c_str());
    }
}
/*--------------------------------------------------------------------------*/
void closeMetanetFromScilab()
{
    /* Call the java implementation */
    try
    {
        Metanet::closeMetanetFromScilab(getScilabJavaVM());
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "closeMetanetFromScilab",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "closeMetanetFromScilab",
                 exception.whatStr().c_str());
    }
}
/*--------------------------------------------------------------------------*/
void loadMetanetFile(char const * filename)
{
    /* Call the java implementation */
    try
    {
        Metanet::loadIntoScilab(getScilabJavaVM(), filename);
    }
    catch (GiwsException::JniCallMethodException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "metanet_load",
                 exception.getJavaDescription().c_str());
    }
    catch (GiwsException::JniException &exception)
    {
        Scierror(999, "%s: %s\n",
                 "metanet_load",
                 exception.whatStr().c_str());
    }
}
