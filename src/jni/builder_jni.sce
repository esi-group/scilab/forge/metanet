//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
// Copyright (C) 2010-2012 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// ====================================================================
function builder_jni()
    CURRENTPATH = strsubst(get_absolute_file_path("builder_jni.sce"), "\", "/");
    FILES_CPP = ["Metanet.cpp", "GiwsException.cpp", "callMetanet.cpp"];
    LD_FLAGS = [];
    C_FLAGS = [];

    INCLUDES_PATHS = "-I" + CURRENTPATH ..
                   + " -I" + CURRENTPATH + "../../includes/";

    if getos() == "Windows" then
        C_FLAGS = INCLUDES_PATHS + " -Od";
        LD_FLAGS = "";
    else

        // Source tree version
        if isdir(SCI + "/modules/core/includes/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I" + SCI + "/modules/jvm/includes/" ..
                           + " -I" + SCI + "/modules/localization/includes/";
        end

        // Binary version
        if isdir(SCI + "/../../include/scilab/core/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I" + SCI + "/../../include/scilab/jvm/" ..
                           + " -I" + SCI + "/../../include/scilab/" ..
                                         + "localization/";
        end

        // System version (ie: /usr/include/scilab/)
        if isdir("/usr/include/scilab/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I/usr/include/scilab/jvm/" ..
                           + " -I/usr/include/scilab/localization/";
        end

        C_FLAGS = INCLUDES_PATHS;

    end

    tbx_build_src("metanet_jni", FILES_CPP, "c", ..
                  CURRENTPATH , "", LD_FLAGS, C_FLAGS);
endfunction
// ====================================================================
builder_jni();
clear builder_jni;
// ====================================================================
