// ====================================================================
// Allan CORNET - DIGITEO - 2009-2012
// ====================================================================
function builder_fortran()
  files_src_fortran = [
    'arbor.f',
    'compfc.f',
    'ford.f',
    'knapsk.f',
    'prim1.f',
    'bandred.f',
    'compmat.f',
    'fordfulk.f',
    'l2que.f',
    'relax.f',
    'bmatch.f',
    'deumesh.f',
    'frang.f',
    'match.f',
    'seed.f',
    'busack.f',
    'dfs.f',
    'frmtrs.f',
    'mesh2b.f',
    'tconex.f',
    'carete.f',
    'dfs1.f',
    'ftrans.f',
    'meshmesh.f',
    'visitor.f',
    'cent.f',
    'dfs2.f',
    'getran.f',
    'minty.f',
    'cfc.f',
    'diam.f',
    'hamil.f',
    'mintyq.f',
    'chcm.f',
    'dijkst.f',
    'hullcvex.f',
    'pcchna.f',
    'clique.f',
    'eclat.f',
    'johns.f',
    'permuto.f',
    'clique1.f',
    'flomax.f',
    'kilter.f',
    'prfmatch.f',
    'compc.f',
    'floqua.f',
    'kiltq.f',
    'prim.f'];

  src_fortran_path = get_absolute_file_path('builder_fortran.sce');

  LDFLAGS="";
  if getos()<>"Windows" then
    LDFLAGS="-lgfortran";
  end

  tbx_build_src('metanet_f', files_src_fortran, 'f', ..
              src_fortran_path, "", LDFLAGS);

endfunction
// ====================================================================
builder_fortran();
clear builder_fortran;
// ====================================================================

