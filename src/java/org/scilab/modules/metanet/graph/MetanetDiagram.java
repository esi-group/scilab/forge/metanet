/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.graph;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.scilab.modules.commons.xml.ScilabTransformerFactory;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.gui.bridge.filechooser.SwingScilabFileChooser;
import org.scilab.modules.gui.filechooser.ScilabFileChooser;
import org.scilab.modules.gui.messagebox.ScilabModalDialog;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.AnswerOption;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.ButtonType;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.IconType;
import org.scilab.modules.gui.utils.SciFileFilter;
import org.scilab.modules.metanet.Metanet;
import org.scilab.modules.metanet.MetanetTab;
import org.scilab.modules.metanet.configuration.ConfigurationManager;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.edge.MetanetEdgeStyle;
import org.scilab.modules.metanet.graph.swing.GraphComponent;
import org.scilab.modules.metanet.io.GraphReader;
import org.scilab.modules.metanet.io.MetanetCodec;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.DefaultNode;
import org.scilab.modules.metanet.utils.MetanetDialogs;
import org.scilab.modules.metanet.utils.MetanetEvent;
import org.scilab.modules.metanet.utils.MetanetFileType;
import org.scilab.modules.metanet.utils.MetanetMessages;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxGraphModel.mxChildChange;
import com.mxgraph.model.mxIGraphModel.mxAtomicGraphModelChange;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxCellOverlay;
import com.mxgraph.swing.util.mxICellOverlay;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxStyleRegistry;

public class MetanetDiagram extends ScilabGraph {

    /**
     * Constructor
     */

    private final String version = "5.0.1";

    boolean directed;

    DefaultNode defaultNode;
    BasicEdge defaultEdge;

    public MetanetDiagram() {
        super();
        mxCodec codec = new mxCodec();

        try {
            final TransformerFactory tranFactory = ScilabTransformerFactory.newInstance();
            final Transformer aTransformer = tranFactory.newTransformer();

            // path will be added to classpath within metanet.start
            final StreamSource src = new StreamSource(ClassLoader.getSystemResource("Metanet-style.xml").getPath());
            final DOMResult result = new DOMResult();
            aTransformer.transform(src, result);

            codec.decode(result.getNode().getFirstChild(), getStylesheet());
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        setComponent(new GraphComponent(this));

        directed = true;
        defaultEdge = new BasicEdge(directed);
        defaultNode = new DefaultNode();

        getAsComponent().setToolTips(true);
        mxConstants.LINE_ARCSIZE = 150;
        // Forbid pending edges.
        setAllowDanglingEdges(false);

        //
        // setCloneInvalidEdges(false);
        setCloneInvalidEdges(true);

        setLabelsVisible(true);
        setHtmlLabels(false);

        setConnectableEdges(false);

        ((mxCell) getDefaultParent()).setId((new UID()).toString());
        ((mxCell) getModel().getRoot()).setId((new UID()).toString());

        /* register a new style for edge */
        mxStyleRegistry.putValue("metanetEdge", new MetanetEdgeStyle());
    }

    /**
     * Install all needed Listeners.
     */
    public void installListeners() {

        // Property change Listener
        // Will say if a diagram has been modified or not.
        getAsComponent().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent e) {
                if (e.getPropertyName().compareTo("modified") == 0) {
                    if ((Boolean) e.getOldValue() != (Boolean) e.getNewValue()) {
                        updateTabTitle();
                    }
                }
            }
        });

        // Add a listener to track when model is changed
        getModel().addListener(mxEvent.CHANGE, new ModelTracker());
        // Track when cells are added.
        addListener(mxEvent.CELLS_ADDED, new CellAddedTracker(this));
        addListener(mxEvent.REMOVE_CELLS, new CellRemovedTracker(this));
        // Track when we have to force a Block value
        addListener(MetanetEvent.FORCE_CELL_VALUE_UPDATE, new ForceCellValueUpdate());
        // Update the blocks view on undo/redo
        getUndoManager().addListener(mxEvent.UNDO, new UndoUpdateTracker());
        getUndoManager().addListener(mxEvent.REDO, new UndoUpdateTracker());
    }

    /**
     * CellAddedTracker Called when mxEvents.CELLS_ADDED is fired.
     */
    private class CellAddedTracker implements mxIEventListener {
        private final MetanetDiagram diagram;

        /**
         * @param diagram
         *            diagram
         */
        public CellAddedTracker(MetanetDiagram diagram) {
            this.diagram = diagram;
        }

        @Override
        public void invoke(Object source, mxEventObject evt) {
            Object[] cells = (Object[]) evt.getProperty("cells");

            diagram.getModel().beginUpdate();
            for (int i = 0; i < cells.length; ++i) {

                // ((mxCell) cells[i]).setId((new UID()).toString());

                if (cells[i] instanceof BasicNode) {
                    // Update parent on cell addition
                    ((BasicNode) cells[i]).setParentDiagram(diagram);
                }
            }
            // fireEvent(XcosEvent.FORCE_CELL_VALUE_UPDATE, new
            // mxEventObject(new Object[] {cells}));
            diagram.getModel().endUpdate();
        }
    }

    /**
     * CellAddedTracker Called when mxEvents.CELLS_ADDED is fired.
     */
    private class CellRemovedTracker implements mxIEventListener {
        private final MetanetDiagram diagram;

        /**
         * @param diagram
         *            diagram
         */
        public CellRemovedTracker(MetanetDiagram diagram) {
            this.diagram = diagram;
        }

        @Override
        public void invoke(Object source, mxEventObject evt) {
            Object[] cells = (Object[]) evt.getProperty("cells");

            diagram.getModel().beginUpdate();
            for (int i = 0; i < cells.length; ++i) {

                // ((mxCell) cells[i]).setId((new UID()).toString());

                if (cells[i] instanceof BasicEdge) {
                    // Update parent on cell addition
                    diagram.refresh();
                }
            }
            // fireEvent(XcosEvent.FORCE_CELL_VALUE_UPDATE, new
            // mxEventObject(new Object[] {cells}));
            diagram.getModel().endUpdate();
        }
    }

    /**
     * ForceCellValueUpdate Called when we want a block content to update.
     */
    private class ForceCellValueUpdate implements mxIEventListener {
        /**
         * Constructor
         */
        public ForceCellValueUpdate() {
            super();
        }

        @Override
        public void invoke(Object source, mxEventObject evt) {
            Object[] cells = (Object[]) evt.getProperty("cells");

            getModel().beginUpdate();

            for (int i = 0; i < cells.length; ++i) {

                Object cell = cells[i];

                if (cell instanceof BasicNode) {

                    // mxRectangle preferedSize = getPreferredSizeForCell(cell);
                    mxGeometry cellSize = ((mxCell) cell).getGeometry();

                    ((mxCell) cell).setGeometry(new mxGeometry(cellSize.getX(), cellSize.getY(), cellSize.getWidth(), cellSize.getHeight()));
                    cellsResized(new Object[] { cell }, new mxRectangle[] { ((mxCell) cell).getGeometry() });
                }
            }
            getModel().endUpdate();
            refresh();
        }
    }

    /**
     * modelTracker Called when mxEvents.CHANGE occurs on a model
     */
    private class ModelTracker implements mxIEventListener {
        /**
         * Constructor
         */
        public ModelTracker() {
            super();
        }

        @Override
        public void invoke(Object source, mxEventObject evt) {

            List<mxAtomicGraphModelChange> changes = (List<mxAtomicGraphModelChange>) (evt.getProperty("changes"));
            List<Object> objects = new ArrayList<Object>();
            getModel().beginUpdate();
            for (int i = 0; i < changes.size(); ++i) {
                if (changes.get(i) instanceof mxChildChange) {

                    if (((mxChildChange) changes.get(i)).getChild() instanceof BasicNode) {
                        BasicNode currentCell = (BasicNode) ((mxChildChange) changes.get(i)).getChild();
                        objects.add(currentCell);
                    }
                }
            }
            if (!objects.isEmpty()) {
                Object[] firedCells = new Object[objects.size()];
                for (int j = 0; j < objects.size(); ++j) {
                    firedCells[j] = objects.get(j);
                }
                // fireEvent(XcosEvent.FORCE_CELL_RESHAPE, new mxEventObject(new
                // Object[] {firedCells}));
                fireEvent(new mxEventObject(MetanetEvent.FORCE_CELL_VALUE_UPDATE, "cells", firedCells));
            }
            getModel().endUpdate();
        }
    }

    /**
     * Update the modified block on undo/redo
     */
    private class UndoUpdateTracker implements mxIEventListener {
        /**
         * Constructor
         */
        public UndoUpdateTracker() {
            super();
        }

        @Override
        public void invoke(Object source, mxEventObject evt) {

            refresh();
        }
    };

    @Override
    public Object addEdge(Object edge, Object parent, Object source, Object target, Integer index) {

        BasicEdge newEdge = (BasicEdge) super.addEdge(new BasicEdge(defaultEdge), parent, source, target, index);

        return newEdge;

    }

    /**
     * Find the object corresponding to the given uid and display a warning
     * message.
     * 
     * @param uid
     *            - A String as UID.
     * @param message
     *            - The message to display.
     */
    public void warnObjectByUID(final String uid, final String message) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    final mxGraphModel diagramModel = (mxGraphModel) getModel();

                    final Object cell = diagramModel.getCell(uid);

                    if (message.equals("select")) {
                        addSelectionCell(cell);
                    } else if (message.equals("unselect")) {
                        removeSelectionCell(cell);
                    }
                }
            });
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load a file with different method depending on it extension
     * 
     * @param theFile
     *            File to load
     * @param wait
     *            wait end transform
     */
    public boolean transformAndLoadFile(File theFile, boolean wait) {
        final File fileToLoad = theFile;
        final MetanetFileType filetype = MetanetFileType.findFileType(fileToLoad);
        boolean result = false;

        switch (filetype) {
        case GRAPH:
            if (wait) {
                File newFile;
                newFile = filetype.exportToHdf5(fileToLoad);
                result = transformAndLoadFile(newFile, wait);
            } else {
                Thread transformAction = new Thread() {
                    @Override
                    public void run() {
                        File newFile;
                        newFile = filetype.exportToHdf5(fileToLoad);
                        transformAndLoadFile(newFile, false);
                    }
                };
                transformAction.start();
                result = true;
            }
            break;

        case XGRAPH:
            Document document = loadMetanetDocument(theFile.getAbsolutePath());
            if (document == null) {
                MetanetDialogs.couldNotLoadFile(this);
                return false;
            }

            MetanetCodec codec = new MetanetCodec(document);

            if (getModel().getChildCount(getDefaultParent()) == 0) {
                codec.decode(document.getDocumentElement(), this);
                setModified(false);
                setSavedFile(theFile);
                setTitle(theFile.getName().substring(0, theFile.getName().lastIndexOf('.')));
                setChildrenParentDiagram();
            } else {
                MetanetDiagram metanetDiagram = Metanet.createANotShownDiagram();
                metanetDiagram.info(MetanetMessages.LOADING_DIAGRAM);
                codec.decode(document.getDocumentElement(), metanetDiagram);
                metanetDiagram.setModified(false);
                metanetDiagram.setSavedFile(theFile);
                metanetDiagram.setTitle(theFile.getName().substring(0, theFile.getName().lastIndexOf('.')));
                setChildrenParentDiagram(metanetDiagram);
                MetanetTab.restore(metanetDiagram);
                metanetDiagram.info(MetanetMessages.EMPTY_INFO);
            }

            result = true;
            break;

        case HDF5:
            openDiagram(GraphReader.readDiagramFromScilab(fileToLoad.getAbsolutePath()));
            generateUID();
            setModified(false);
            result = true;
            break;

        default:
            // TODO implement something more graphics
            System.out.println("Error while opening");
            break;
        }

        return result;
    }

    /**
     * generate unique id to all blocks in diagram
     */
    public void generateUID() {
        for (int i = 0; i < getModel().getChildCount(getDefaultParent()); ++i) {
            if (getModel().getChildAt(getDefaultParent(), i) instanceof BasicNode) {
                BasicNode node = (BasicNode) getModel().getChildAt(getDefaultParent(), i);

                if (node.getId() == null || node.getId().compareTo("") == 0) {
                    node.setId();
                }
            }
        }
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        updateTabTitle();
    }

    /**
	 * 
	 */
    public void updateTabTitle() {
        String tabTitle = !isModified() ? getTitle() : "* " + getTitle();

        final MetanetTab tab = MetanetTab.get(this);
        if (tab != null) {
            tab.setName(tabTitle);
            tab.draw();
        }
    }

    /**
     * @param metanetFile
     *            metanet file
     * @return opened document
     */
    static Document loadMetanetDocument(String metanetFile) {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docBuilderFactory.newDocumentBuilder();
            return docBuilder.parse(metanetFile);
        } catch (ParserConfigurationException e) {
            return null;
        } catch (SAXException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Open a Diagram : If current Diagram is empty, open within it else open a
     * new window.
     * 
     * @param diagramm
     */
    public void openDiagram(HashMap<String, Object> diagramm) {
        if (diagramm != null) {
            if (getModel().getChildCount(getDefaultParent()) == 0) {
                loadDiagram(diagramm);
            } else {
                MetanetDiagram metanetDiagram = Metanet.createANotShownDiagram();
                metanetDiagram.loadDiagram(diagramm);
                setChildrenParentDiagram(metanetDiagram);

                MetanetTab.restore(metanetDiagram);
            }
        } else {
            Logger.getLogger(MetanetDiagram.class.getName()).severe("Error while loading diagram");
        }
    }

    /**
     * Read a diagram from an HDF5 file (ask for creation if the file does not
     * exist)
     * 
     * @param diagramFileName
     *            file to open
     */
    public void openDiagramFromFile(String diagramFileName) {
        if (!MetanetTab.focusOnExistingFile(diagramFileName)) {
            File theFile = new File(diagramFileName);
            info(MetanetMessages.LOADING_DIAGRAM);

            if (theFile.exists()) {
                transformAndLoadFile(theFile, false);
            } else {
                AnswerOption answer = ScilabModalDialog.show(MetanetTab.get(this), String.format(MetanetMessages.FILE_DOESNT_EXIST, theFile.getAbsolutePath()),
                        MetanetMessages.METANET, IconType.QUESTION_ICON, ButtonType.YES_NO);

                if (answer == AnswerOption.YES_OPTION) {
                    try {
                        FileWriter writer = new FileWriter(diagramFileName);
                        writer.write("");
                        writer.flush();
                        writer.close();
                        setSavedFile(new File(diagramFileName));
                        setTitle(theFile.getName().substring(0, theFile.getName().lastIndexOf('.')));
                    } catch (IOException ioexc) {
                        JOptionPane.showMessageDialog(this.getAsComponent(), ioexc);
                    }
                }
            }
            info(MetanetMessages.EMPTY_INFO);
            getUndoManager().clear();
        }
    }

    /**
     * Update all the children of the current graph.
     */
    public void setChildrenParentDiagram() {
        setChildrenParentDiagram(this);
    }

    /**
     * For each Node in the argument, call its setParentDiagram method
     * 
     * @param diagram
     *            The new parent of the nodes.
     */
    private void setChildrenParentDiagram(MetanetDiagram diagram) {
        for (int i = 0; i < diagram.getModel().getChildCount(diagram.getDefaultParent()); i++) {
            mxCell cell = (mxCell) diagram.getModel().getChildAt(diagram.getDefaultParent(), i);
            if (cell instanceof BasicNode) {
                BasicNode node = (BasicNode) cell;
                node.setParentDiagram(diagram);

            }
        }
    }

    /**
     * Load a Diagramm structure into current window.
     * 
     * @param diagramm
     *            diagram structure
     */
    public void loadDiagram(HashMap<String, Object> diagram) {
        List<BasicNode> allNodes = (List<BasicNode>) diagram.get("Nodes");
        List<BasicEdge> allEdges = (List<BasicEdge>) diagram.get("Edges");
        DefaultNode defaultNode = (DefaultNode) diagram.get("DefaultNode");
        BasicEdge defaultEdge = (BasicEdge) diagram.get("DefaultEdge");
        boolean directed = (Boolean) diagram.get("Directed");
        HashMap<String, Object> properties = (HashMap<String, Object>) diagram.get("Properties");

        Object[] objs = new Object[allNodes.size() + allEdges.size()];
        getModel().beginUpdate();
        for (int i = 0; i < allNodes.size(); ++i) {
            objs[i] = allNodes.get(i);
        }

        for (int i = 0; i < allEdges.size(); ++i) {
            objs[i + allNodes.size()] = allEdges.get(i);
        }

        addCells(objs);
        getModel().endUpdate();

        // this.setTitle(fileToLoad);
        // this.getParentTab().setName(fileToLoad);
        setDirected(directed);
        setDefaultEdge(defaultEdge);
        setDefaultNode(defaultNode);

        setTitle((String) properties.get("title"));
        updateTabTitle();
        // Clear all undo events in Undo Manager
        getUndoManager().clear();
        setModified(false);
        refresh();

    }

    /**
     * @return save status
     */
    public boolean saveDiagram() {
        boolean isSuccess = false;

        String f = null;
        if (getSavedFile() != null) {
            try {
                f = getSavedFile().getCanonicalPath();
            } catch (IOException e) {
                Logger.getLogger(MetanetDiagram.class.getName()).severe(e.toString());
            }
        }

        isSuccess = saveDiagramAs(f);

        if (isSuccess) {
            setModified(false);
        }
        return isSuccess;
    }

    /**
     * @param fileName
     *            diagram filename
     * @return save status
     */
    public boolean saveDiagramAs(String fileName) {

        boolean isSuccess = false;
        String writeFile = fileName;
        info(MetanetMessages.SAVING_DIAGRAM);
        if (fileName == null) {
            // Choose a filename
            SwingScilabFileChooser fc = ((SwingScilabFileChooser) ScilabFileChooser.createFileChooser().getAsSimpleFileChooser());
            fc.setTitle(MetanetMessages.SAVE_AS);
            fc.setUiDialogType(JFileChooser.SAVE_DIALOG);
            fc.setMultipleSelection(false);
            if (this.getSavedFile() != null) {
                fc.setSelectedFile(this.getSavedFile());
            }

            SciFileFilter metanetFilter = new SciFileFilter("*.xgraph", null, 0);
            SciFileFilter allFilter = new SciFileFilter("*.*", null, 1);
            fc.addChoosableFileFilter(metanetFilter);
            fc.addChoosableFileFilter(allFilter);
            fc.setFileFilter(metanetFilter);

            fc.setAcceptAllFileFilterUsed(false);
            fc.displayAndWait();

            if (fc.getSelection() == null || fc.getSelection().length == 0 || fc.getSelection()[0].equals("")) {
                info(MetanetMessages.EMPTY_INFO);
                return isSuccess;
            }
            writeFile = fc.getSelection()[0];
        }
        /* Extension checks */
        File file = new File(writeFile);
        if (!file.exists()) {
            String extension = writeFile.substring(writeFile.lastIndexOf('.') + 1);

            if (extension.equals(writeFile)) {
                /* No extension given --> .xgraph added */
                writeFile += ".xgraph";
            }
        }

        MetanetCodec codec = new MetanetCodec();
        try {
            final TransformerFactory tranFactory = ScilabTransformerFactory.newInstance();
            final Transformer aTransformer = tranFactory.newTransformer();

            final DOMSource src = new DOMSource(codec.encode(this));
            final StreamResult result = new StreamResult(new File(writeFile));
            aTransformer.transform(src, result);

            isSuccess = true;
        } catch (TransformerException e) {
            Logger.getLogger(MetanetDiagram.class.getName()).severe(e.toString());
            isSuccess = false;
        }

        if (isSuccess) {
            this.setSavedFile(new File(writeFile));
            File theFile = new File(writeFile);
            setTitle(theFile.getName().substring(0, theFile.getName().lastIndexOf('.')));
            ConfigurationManager.getInstance().addToRecentFiles(writeFile);
            ConfigurationManager.getInstance().saveConfig();
            setModified(false);
        } else {
            MetanetDialogs.couldNotSaveFile(this);
        }
        info(MetanetMessages.EMPTY_INFO);
        return isSuccess;
    }

    /**
     * 
     * @return
     */

    public boolean isDirected() {
        return directed;
    }

    /**
     * 
     * @param directed
     */

    public void setDirected(boolean directed) {
        this.directed = directed;
    }

    @Override
    public boolean isCellResizable(Object cell) {
        final boolean isVertex = getModel().isVertex(cell);
        final mxCellState state = getView().getState(cell, false);

        if (!isVertex || state == null) {
            return false;
        }

        final double scale = getView().getScale();
        final double sw = state.getWidth() * scale;
        final double sh = state.getHeight() * scale;
        final boolean isResizable = sh * sw > (20.0 * 20.0);

        return isResizable && super.isCellResizable(cell);
    }

    /**
     * 
     * @return
     */

    public DefaultNode getDefaultNode() {
        return defaultNode;
    }

    /**
     * 
     * @param defaultNode
     */

    public void setDefaultNode(DefaultNode defaultNode) {
        this.defaultNode = defaultNode;
    }

    /**
     * 
     * @return
     */

    public BasicEdge getDefaultEdge() {
        return defaultEdge;
    }

    /**
     * 
     * @param defaultEdge
     */

    public void setDefaultEdge(BasicEdge defaultEdge) {
        this.defaultEdge = defaultEdge;
    }

    /**
     * Display the message in info bar.
     * 
     * @param message
     *            Informations
     */
    public void info(final String message) {
        final MetanetTab tab = MetanetTab.get(this);
        if (tab != null && tab.getInfoBar() != null) {
            tab.getInfoBar().setText(message);
        }
    }

    /**
     * @return current version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Returns the tooltip to be used for the given cell.
     * 
     * @param cell
     *            block
     * @return cell tooltip
     */
    @Override
    public String getToolTipForCell(Object cell) {
        if (cell instanceof BasicNode) {
            return ((BasicNode) cell).getToolTipText();
        } else if (cell instanceof BasicEdge) {
            return ((BasicEdge) cell).getToolTipText();
        }
        return "";
    }

    /**
     * Show some message on top of the indexed cell
     * 
     * @param nodes
     *            true, if cells are nodes
     * @param edges
     *            true, if cells are edges
     * @param indexes
     *            index of the cells
     * @param clearPrevious
     *            true to clear the previous message, append otherwise
     * @param message
     *            the message to display
     */
    public void show(final boolean nodes, final boolean edges, final double[] indexes, final boolean clearPrevious, final String message) {
        final Collection<Object> objects = mxGraphModel.filterDescendants(getModel(), new FilterByIndexes(nodes, edges, indexes));
        final mxGraphComponent comp = getAsComponent();

        /*
         * Define a dynamic overlay to query the node data on demand. If no
         * message, change strokeWidth style to bold.
         */
        final mxICellOverlay overlay = new MetanetObjectOverlay(message, comp);
        final boolean updateStyle = message.equals("nothing");

        if (updateStyle) {
            if (clearPrevious) {
                setCellStyles(mxConstants.STYLE_STROKEWIDTH, "1.0", mxGraphModel.getChildCells(getModel(), getDefaultParent(), nodes, edges));
            }

            setCellStyles(mxConstants.STYLE_STROKEWIDTH, "3.0", objects.toArray());
        } else {
            // Add a cell overlay, if a message is present
            for (Object cell : objects) {
                if (clearPrevious) {
                    comp.removeCellOverlays(cell);
                }

                if (overlay != null) {
                    comp.addCellOverlay(cell, overlay);
                }
            }
        }
    }

    private static class FilterByIndexes implements mxGraphModel.Filter {
        final boolean nodes;
        final boolean edges;
        final double[] indexes;

        public FilterByIndexes(final boolean nodes, final boolean edges, final double[] indexes) {
            this.nodes = nodes;
            this.edges = edges;
            this.indexes = indexes;

            Arrays.sort(indexes);
        }

        @Override
        public boolean filter(Object object) {

            if (nodes && object instanceof BasicNode) {
                return Arrays.binarySearch(indexes, ((BasicNode) object).getOrdering()) >= 0;
            } else if (edges && object instanceof BasicEdge) {
                return Arrays.binarySearch(indexes, ((BasicEdge) object).getOrdering()) >= 0;
            }

            return false;
        }
    }

    private static class MetanetObjectOverlay extends mxCellOverlay {
        final mxGraphComponent comp;
        final String message;

        public MetanetObjectOverlay(final String message, final mxGraphComponent comp) {
            super(mxGraphComponent.DEFAULT_WARNING_ICON, message);

            defaultOverlap = 0;

            this.message = message;
            this.comp = comp;
        }

        @Override
        public String getToolTipText(MouseEvent e) {
            final Object o = comp.getCellAt(e.getX(), e.getY());

            final Object value;
            if (o instanceof BasicNode) {
                value = ((BasicNode) o).getDataFieldsValue(message);
            } else if (o instanceof BasicEdge) {
                value = ((BasicEdge) o).getDataFieldsValue(message);
            } else {
                value = null;
            }

            if (value != null) {
                return String.valueOf(value);
            } else {
                return message;
            }
        }
    }
}
