/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2012 - Scilab Enterprises - Clement DAVID
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.graph.swing;

import java.awt.Color;
import java.util.EventObject;

import org.scilab.modules.graph.ScilabComponent;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.edge.actions.EdgeParametersAction;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.graph.swing.handler.ConnectionHandler;
import org.scilab.modules.metanet.graph.swing.handler.GraphHandler;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.actions.NodeParametersAction;

import com.mxgraph.swing.handler.mxConnectionHandler;
import com.mxgraph.swing.handler.mxGraphHandler;

public class GraphComponent extends ScilabComponent {

    public GraphComponent(final MetanetDiagram graph) {
        super(graph);

        initComponent();
    }

    private void initComponent() {
        setToolTips(true);

        setTolerance(1);

        getViewport().setOpaque(false);
        setBackground(Color.WHITE);
    }

    @Override
    public void startEditingAtCell(Object cell, EventObject evt) {
        if (cell instanceof BasicNode) {
            NodeParametersAction.editNodeBox((BasicNode) cell, (MetanetDiagram) getGraph());
        } else if (cell instanceof BasicEdge) {
            EdgeParametersAction.editEdgeBox((BasicEdge) cell, (MetanetDiagram) getGraph());
        }
    }

    /**
     * @return a new {@link ConnectionHandler} instance
     * @see com.mxgraph.swing.mxGraphComponent#createConnectionHandler()
     */
    @Override
    protected mxConnectionHandler createConnectionHandler() {
        return new ConnectionHandler(this);
    }

    /**
     * @return a new {@link GraphHandler} instance
     * @see com.mxgraph.swing.mxGraphComponent#createConnectionHandler()
     */
    @Override
    protected mxGraphHandler createGraphHandler() {
        return new GraphHandler(this);
    }
}
