/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2012 - Scilab Enterprises - Clement DAVID
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.graph.swing.handler;

import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;

import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.graph.swing.GraphComponent;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.swing.handler.mxGraphHandler;

public class GraphHandler extends mxGraphHandler {

    public GraphHandler(GraphComponent graphComponent) {
        super(graphComponent);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        final Object cell = graphComponent.getCellAt(e.getX(), e.getY());
        final MetanetDiagram graph = (MetanetDiagram) graphComponent.getGraph();

        // Double Click within empty diagram Area
        if (e.getClickCount() >= 2 && SwingUtilities.isLeftMouseButton(e) && cell == null) {
            BasicNode newNode = new BasicNode(graph.getDefaultNode());

            newNode.getGeometry().setX((e.getX() / graph.getView().getScale()) - newNode.getGeometry().getWidth() / 2.0);
            newNode.getGeometry().setY((e.getY() / graph.getView().getScale()) - newNode.getGeometry().getHeight() / 2.0);
            graphComponent.getGraph().addCell(newNode);
            return;
        }

        // Double Click within some component
        if (e.getClickCount() >= 2 && SwingUtilities.isLeftMouseButton(e) && cell != null) {

        }

        // Ctrl + Shift + Right Middle Click : for debug !!
        if (e.getClickCount() >= 2 && SwingUtilities.isMiddleMouseButton(e) && e.isShiftDown() && e.isControlDown()) {
        }

        // Context menu
        if ((e.getClickCount() == 1 && SwingUtilities.isRightMouseButton(e)) || e.isPopupTrigger() || MetanetMessages.isMacOsPopupTrigger(e)) {

            if (cell == null) {

            } else {
                // Display object context menu
                if (cell instanceof BasicNode) {
                    BasicNode node = (BasicNode) cell;
                    node.openContextMenu((MetanetDiagram) graphComponent.getGraph());
                }
                if (cell instanceof BasicEdge) {
                    BasicEdge edge = (BasicEdge) cell;
                    edge.openContextMenu((MetanetDiagram) graphComponent.getGraph());
                }
            }
        }

    }
}
