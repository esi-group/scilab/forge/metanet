/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.node;

import java.awt.MouseInfo;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.CopyAction;
import org.scilab.modules.graph.actions.CutAction;
import org.scilab.modules.graph.actions.DeleteAction;
import org.scilab.modules.graph.actions.base.DefaultAction;
import org.scilab.modules.graph.utils.ScilabGraphConstants;
import org.scilab.modules.gui.bridge.contextmenu.SwingScilabContextMenu;
import org.scilab.modules.gui.contextmenu.ContextMenu;
import org.scilab.modules.gui.contextmenu.ScilabContextMenu;
import org.scilab.modules.gui.menu.Menu;
import org.scilab.modules.gui.menu.ScilabMenu;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.MetanetUIDObject;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.node.actions.BorderColorAction;
import org.scilab.modules.metanet.node.actions.NodeParametersAction;
import org.scilab.modules.metanet.utils.MetanetMessages;

import com.mxgraph.model.mxGeometry;

public class BasicNode extends MetanetUIDObject {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    // FIXME: the ordering could be removed if the MetanetModel use a LinkedHashMap instead of an HashMap
    private int ordering;
    // FIXME: should be removed ; stored data should be stored into value as a list of Scilab value 
    private Hashtable<String, Object> data;

    // TODO to replace by a enum
    // if the node is a source / a normal / an exit one
    private int type;
    // FIXME: duplicated among geometry value ; this can be removed safely
    private double diameter;
    // FIXME: the border appears to be a style property and thus should be implemented as it and removed from there
    private double border;
    // FIXME: The name have to be the toString of the 'value' returned by mxICell#getValue()
    //        the default component will use toString() to render the value of a cell
    private String nodeName;

    // FIXME: this member should be removed to clean MVC and follow JGraphX convention
    private transient MetanetDiagram parentDiagram;

    public BasicNode() {
        super();
        nodeName = "";
        data = new Hashtable<String, Object>();
        setStyle("node");
        setVisible(true);
        setVertex(true);
    }

    public BasicNode(BasicNode defaultNode) {
        super();
        nodeName = "";
        data = (Hashtable<String, Object>) defaultNode.getData().clone();
        style = defaultNode.getStyle();
        type = defaultNode.getType();
        diameter = defaultNode.getDiameter();
        border = defaultNode.getBorder();
        nodeName = defaultNode.getNodeName();
        geometry = new mxGeometry(0, 0, diameter, diameter);

        setVisible(true);
        setVertex(true);
    }

    /**
     * @param ordering
     *            order value
     */
    public void setOrdering(int ordering) {
        this.ordering = ordering;
    }

    /**
     * @return order value
     */
    public int getOrdering() {
        return ordering;
    }

    /**
     * @param type
     *            value
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return type value
     */
    public int getType() {
        return type;
    }

    /**
     * @param diameter
     *            value
     */
    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    /**
     * @return diameter value
     */
    public double getDiameter() {
        return diameter;
    }

    /**
     * @param border
     *            value
     */
    public void setBorder(double border) {
        this.border = border;
    }

    /**
     * @return border value
     */
    public double getBorder() {
        return border;
    }

    /**
     * @param nodeName
     *            name of the node value
     */

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * @return nodeName name of the node
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * @param parentDiagram
     *            parent diagram
     */
    public void setParentDiagram(MetanetDiagram parentDiagram) {
        this.parentDiagram = parentDiagram;
    }

    /**
     * @return parent diagram
     */
    public MetanetDiagram getParentDiagram() {
        return parentDiagram;
    }

    /**
	 * 
	 */
    public void addDataField(String fieldName, Object value) {
        data.put(fieldName, value);
    }

    /**
     * 
     * @return
     */
    public String[] getDataFieldsName() {
        return data.keySet().toArray(new String[0]);
    }

    /**
     * 
     * @return
     */
    public Object getDataFieldsValue(String fieldName) {
        return data.get(fieldName);
    }

    /**
     * 
     * @return
     */
    public Object setDataFieldsValue(String fieldName, Object value) {
        return data.put(fieldName, value);
    }

    /**
     * 
     * @return
     */
    public void removeDataField(String fieldName) {
        data.remove(fieldName);
    }

    /**
     * 
     * @return
     */

    public Hashtable<String, Object> getData() {
        return data;
    }

    /**
     * 
     * @return
     */

    public void setData(Hashtable<String, Object> data) {
        this.data = data;
    }

    /**
     * @param graph
     *            parent graph
     */
    public void openContextMenu(ScilabGraph graph) {
        ContextMenu menu = null;
        menu = createContextMenu(graph);
        menu.setVisible(true);
    }

    /**
     * @param graph
     *            parent graph
     * @return context menu
     */
    public ContextMenu createContextMenu(ScilabGraph graph) {
        ContextMenu menu = ScilabContextMenu.createContextMenu();
        Map<Class<? extends DefaultAction>, Menu> menuList = new HashMap<Class<? extends DefaultAction>, Menu>();

        MenuItem value = NodeParametersAction.createMenu(graph);
        menuList.put(NodeParametersAction.class, value);
        menu.add(value);
        /*--- */
        menu.getAsSimpleContextMenu().addSeparator();
        /*--- */
        value = CutAction.cutMenu(graph);
        menuList.put(CutAction.class, value);
        menu.add(value);
        value = CopyAction.copyMenu(graph);
        menuList.put(CopyAction.class, value);
        menu.add(value);
        value = DeleteAction.createMenu(graph);
        menuList.put(DeleteAction.class, value);
        menu.add(value);
        /*--- */
        menu.getAsSimpleContextMenu().addSeparator();
        /*--- */
        Menu format = ScilabMenu.createMenu();
        format.setText(MetanetMessages.FORMAT);
        menu.add(format);
        /*--- */
        format.add(BorderColorAction.createMenu(graph));
        // format.add(FilledColorAction.createMenu(graph));
        /*--- */
        // menu.getAsSimpleContextMenu().addSeparator();
        /*--- */
        // menu.add(ViewDetailsAction.createMenu(graph));

        ((SwingScilabContextMenu) menu.getAsSimpleContextMenu()).setLocation(MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo()
                .getLocation().y);

        customizeMenu(menuList);

        return menu;
    }

    /**
     * Override this to customize contextual menu
     * 
     * @param menuList
     *            list of menu
     */
    protected void customizeMenu(Map<Class<? extends DefaultAction>, Menu> menuList) {
        // To be overridden by sub-classes
    }

    /**
     * 
     * @return
     */

    public String getToolTipText() {
        StringBuffer result = new StringBuffer();
        result.append(ScilabGraphConstants.HTML_BEGIN);

        result.append("Node Name : " + getNodeName() + ScilabGraphConstants.HTML_NEWLINE);

        for (Entry<String, Object> entry : data.entrySet()) {
            String fieldName = entry.getKey();
            Object value = entry.getValue();
            result.append(fieldName + " : " + value + ScilabGraphConstants.HTML_NEWLINE);
        }
        result.append("id : " + getId() + ScilabGraphConstants.HTML_NEWLINE);
        result.append(ScilabGraphConstants.HTML_END);
        return result.toString();
    }

}
