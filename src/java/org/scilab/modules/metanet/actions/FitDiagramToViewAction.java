/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.actions;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import org.scilab.modules.graph.ScilabComponent;
import org.scilab.modules.graph.ScilabGraph;
import org.scilab.modules.graph.actions.base.OneBlockDependantAction;
import org.scilab.modules.gui.menuitem.MenuItem;
import org.scilab.modules.metanet.utils.MetanetMessages;

public class FitDiagramToViewAction extends OneBlockDependantAction {
    public static final String NAME = MetanetMessages.FIT_DIAGRAM_TO_VIEW;
    public static final String SMALL_ICON = "zoom-fit-drawing";
    public static final int MNEMONIC_KEY = 0;
    public static final int ACCELERATOR_KEY = 0;

    /**
     * Constructor
     * 
     * @param scilabGraph
     *            associated diagram
     */
    public FitDiagramToViewAction(ScilabGraph scilabGraph) {
        super(scilabGraph);
    }

    /**
     * Menu to add to the menubar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the menu
     */
    public static MenuItem createMenu(ScilabGraph scilabGraph) {
        return createMenu(scilabGraph, FitDiagramToViewAction.class);
    }

    /**
     * Button to add to the toolbar
     * 
     * @param scilabGraph
     *            associated diagram
     * @return the button
     */
    public static JButton createButton(ScilabGraph scilabGraph) {
        return createButton(scilabGraph, FitDiagramToViewAction.class);
    }

    /**
     * @param e
     *            parameter
     * @see org.scilab.modules.graph.actions.base.DefaultAction#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // If diagram is empty (has one default child) : do nothing.
        if (getGraph(null).getModel().getChildCount(getGraph(null).getDefaultParent()) < 1) {
            return;
        }

        ScilabComponent comp = ((ScilabComponent) getGraph(null).getAsComponent());

        /* Save the configuration */
        double oldZoomFactor = comp.getZoomFactor();

        comp.zoomAndCenterToCells(getGraph(null).getSelectionCells());

        /* Restore previous configuration */
        comp.setZoomFactor(oldZoomFactor);
    }
}
