/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.io;

import org.scilab.modules.graph.io.ScilabObjectCodec;
import org.scilab.modules.metanet.edge.BasicEdge;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.node.BasicNode;
import org.scilab.modules.metanet.node.DefaultNode;
import org.w3c.dom.Document;

import com.mxgraph.io.mxCellCodec;
import com.mxgraph.io.mxCodec;
import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxObjectCodec;

public class MetanetCodec extends mxCodec {
    /**
     * Register usefull codecs and packages for encoding/decoding diagrams
     */
    static {
        // Add all metanet packages
        mxCodecRegistry.addPackage("org.scilab.modules.graph");
        mxCodecRegistry.addPackage("org.scilab.modules.metanet");
        mxCodecRegistry.addPackage("org.scilab.modules.metanet.edge");
        mxCodecRegistry.addPackage("org.scilab.modules.metanet.node");
        mxCodecRegistry.addPackage("org.scilab.modules.metanet.graph");
        // Add 'types' package to have all scilab types known
        mxCodecRegistry.addPackage("org.scilab.modules.types");

        // Types
        ScilabObjectCodec.register();

        final String[] refs = { "parent", "source", "target" };

        //
        String[] diagramIgnore = { "stylesheet", "parentTab", "viewPort", "viewPortMenu", "view", "selectionModel", "savedFile", "multiplicities" };
        mxObjectCodec diagramCodec = new mxObjectCodec(new MetanetDiagram(), diagramIgnore, refs, null);
        mxCodecRegistry.register(diagramCodec);

        //
        mxObjectCodec basicNodeCodec = new mxCellCodec(new BasicNode(), null, refs, null);
        mxCodecRegistry.register(basicNodeCodec);

        mxObjectCodec defaultNodeCodec = new mxCellCodec(new DefaultNode(), new String[] { "geometry" }, refs, null);
        mxCodecRegistry.register(defaultNodeCodec);

        //
        //
        mxObjectCodec basicEdgeCodec = new mxCellCodec(new BasicEdge(), null, refs, null);
        mxCodecRegistry.register(basicEdgeCodec);
    }

    public MetanetCodec() {
        super();
    }

    public MetanetCodec(Document document) {
        super(document);
    }

}
