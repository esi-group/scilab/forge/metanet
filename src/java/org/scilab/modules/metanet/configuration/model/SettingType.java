package org.scilab.modules.metanet.configuration.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * Global configuration.
 * 
 * 
 * <p>
 * Java class for SettingType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="SettingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="recentFiles" type="{}RecentFilesType"/>
 *         &lt;element name="restored" type="{}DocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettingType", propOrder = { "recentFiles", "restored" })
public class SettingType {

    @XmlElement(required = true)
    protected RecentFilesType recentFiles;
    protected List<DocumentType> restored;

    /**
     * Gets the value of the recentFiles property.
     * 
     * @return possible object is {@link RecentFilesType }
     * 
     */
    public RecentFilesType getRecentFiles() {
        return recentFiles;
    }

    /**
     * Sets the value of the recentFiles property.
     * 
     * @param value
     *            allowed object is {@link RecentFilesType }
     * 
     */
    public void setRecentFiles(RecentFilesType value) {
        this.recentFiles = value;
    }

    /**
     * Gets the value of the restored property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the restored property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getRestored().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentType }
     * 
     * 
     */
    public List<DocumentType> getRestored() {
        if (restored == null) {
            restored = new ArrayList<DocumentType>();
        }
        return this.restored;
    }

}
