/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Allan SIMON
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.metanet.configuration;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.scilab.modules.commons.ScilabConstants;
import org.scilab.modules.gui.messagebox.ScilabModalDialog;
import org.scilab.modules.gui.messagebox.ScilabModalDialog.IconType;
import org.scilab.modules.metanet.configuration.model.DocumentType;
import org.scilab.modules.metanet.configuration.model.ObjectFactory;
import org.scilab.modules.metanet.configuration.model.SettingType;
import org.scilab.modules.metanet.configuration.utils.ConfigurationConstants;
import org.scilab.modules.metanet.graph.MetanetDiagram;
import org.scilab.modules.metanet.utils.FileUtils;
import org.scilab.modules.metanet.utils.MetanetMessages;
import org.xml.sax.SAXException;

/**
 * Entry point to manage the configuration
 */
public final class ConfigurationManager {
    private static final String UNABLE_TO_VALIDATE_CONFIG = "Unable to validate the configuration file.\n";
    private static final String MODEL_CLASS_PACKAGE = "org.scilab.modules.metanet.configuration.model";
    private static final String SCHEMA_FILENAME = "MetanetConfiguration.xsd";
    private static final String INSTANCE_FILENAME = "metanet.xml";

    private static ConfigurationManager instance;
    private static Marshaller marshaller;
    private static Unmarshaller unmarshaller;

    private final SettingType settings;
    private final PropertyChangeSupport changeSupport;

    /**
     * Default constructor
     */
    private ConfigurationManager() {
        settings = loadConfig();
        changeSupport = new PropertyChangeSupport(this);
    }

    /**
     * The only manager instance
     * 
     * @return the instance
     */
    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
        }
        return instance;
    }

    /**
     * @return the settings
     */
    public SettingType getSettings() {
        return settings;
    }

    /**
     * Load the configuration file and return the root object.
     * 
     * @return the configuration instance
     */
    public SettingType loadConfig() {
        try {
            if (unmarshaller == null) {
                initUnmarshaller();
            }

            File f;
            try {
                f = new File(ScilabConstants.SCIHOME.getAbsoluteFile() + System.getProperty("file.separator") + INSTANCE_FILENAME);

                if (!f.exists()) {
                    File base = new File(ClassLoader.getSystemResource(INSTANCE_FILENAME).getPath());
                    FileUtils.forceCopy(base, f);
                }

                final JAXBElement<SettingType> conf = unmarshaller.unmarshal(new StreamSource(f), SettingType.class);
                return conf.getValue();
            } catch (JAXBException e) {
                Logger.getLogger(ConfigurationManager.class.getName()).warning("user configuration file is not valid.\n" + "Switching to the default one." + e);

                ScilabModalDialog.show(null, MetanetMessages.ERR_CONFIG_INVALID, MetanetMessages.METANET_ERROR, IconType.ERROR_ICON);

                try {
                    f = new File(ClassLoader.getSystemResource(INSTANCE_FILENAME).getPath());

                    final JAXBElement<SettingType> conf = unmarshaller.unmarshal(new StreamSource(f), SettingType.class);
                    return conf.getValue();
                } catch (JAXBException ex) {
                    Logger.getLogger(ConfigurationManager.class.getName()).severe("base configuration file corrupted.\n" + ex);
                    return null;
                }
            }

        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Initialize the shared unmarshaller instance
     * 
     * @throws JAXBException
     *             when an unsupported error has occured
     */
    private void initUnmarshaller() throws JAXBException {
        final String schemaPath = ClassLoader.getSystemResource(SCHEMA_FILENAME).getPath();
        JAXBContext jaxbContext = JAXBContext.newInstance(MODEL_CLASS_PACKAGE);
        unmarshaller = jaxbContext.createUnmarshaller();

        try {
            Schema schema;
            schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(schemaPath));
            unmarshaller.setSchema(schema);
        } catch (SAXException e) {
            Logger.getLogger(ConfigurationManager.class.getName()).severe(UNABLE_TO_VALIDATE_CONFIG + e);
        }
    }

    /**
     * Save {@link #settings} on the configuration file.
     */
    public void saveConfig() {
        try {
            if (marshaller == null) {
                initMarshaller();
            }

            File f;
            try {
                f = new File(ScilabConstants.SCIHOME.getAbsoluteFile() + System.getProperty("file.separator") + INSTANCE_FILENAME);
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.marshal(new ObjectFactory().createSettings(getSettings()), f);
            } catch (JAXBException e) {
                Logger.getLogger(ConfigurationManager.class.getName()).severe("Unable to save user configuration file.\n" + e);
            }

        } catch (JAXBException e) {
            e.printStackTrace();
            return;
        }
    }

    /**
     * Initialize the shared marshaller instance
     * 
     * @throws JAXBException
     *             when an unsupported error has occured
     */
    private void initMarshaller() throws JAXBException {
        final String schemaPath = ClassLoader.getSystemResource(SCHEMA_FILENAME).getPath();
        JAXBContext jaxbContext = JAXBContext.newInstance(MODEL_CLASS_PACKAGE);
        marshaller = jaxbContext.createMarshaller();

        try {
            Schema schema;
            schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File(schemaPath));
            marshaller.setSchema(schema);
        } catch (SAXException e) {
            Logger.getLogger(ConfigurationManager.class.getName()).warning(UNABLE_TO_VALIDATE_CONFIG + e);
        }
    }

    /**
     * Update the configuration by adding a file.
     * 
     * This method doesn't perform the save.
     * 
     * @param string
     *            the file path to add
     */
    public void addToRecentFiles(String string) {
        List<DocumentType> files = getSettings().getRecentFiles().getDocument();

        /*
         * Create the url
         */
        String url;
        try {
            url = new File(string).toURI().toURL().toString();
        } catch (MalformedURLException e) {
            Logger.getLogger(ConfigurationManager.class.getName()).severe(e.toString());
            return;
        }

        /*
         * Create the date
         */
        DatatypeFactory factory;
        try {
            factory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            Logger.getLogger(ConfigurationManager.class.getName()).severe(e.toString());
            return;
        }

        /*
         * Initialize the new element
         */
        DocumentType element = (new ObjectFactory()).createDocumentType();
        element.setUrl(url);
        element.setDate(factory.newXMLGregorianCalendar(new GregorianCalendar()));

        /*
         * Create an arrays sorted by name.
         */
        DocumentType[] perNameSortedFiles = files.toArray(new DocumentType[files.size()]);
        Arrays.sort(perNameSortedFiles, ConfigurationConstants.FILENAME_COMPARATOR);

        /*
         * Insert the element
         */
        DocumentType oldElement = null;
        int search = Arrays.binarySearch(perNameSortedFiles, element, ConfigurationConstants.FILENAME_COMPARATOR);

        if (search >= 0) {
            // Element found, remove the old element
            oldElement = perNameSortedFiles[search];
            files.remove(oldElement);
        } else {
            // Element not found, remove the last element if
            // there is no more place.
            if (files.size() == ConfigurationConstants.MAX_RECENT_FILES) {
                oldElement = files.remove(ConfigurationConstants.MAX_RECENT_FILES - 1);
            }
        }

        files.add(0, element);

        /*
         * Fire the associated event
         */
        firePropertyChange(ConfigurationConstants.RECENT_FILES_CHANGED, oldElement, element);
    }

    public void updateOpenedTab(MetanetDiagram graph) {
        final List<DocumentType> files = getSettings().getRestored();

        final URL url;
        if (graph.getSavedFile() != null) {
            try {
                url = graph.getSavedFile().toURI().toURL();
            } catch (MalformedURLException e) {
                return;
            }
        } else {
            return;
        }

        DocumentType doc = (new ObjectFactory()).createDocumentType();
        doc.setUrl(url.toString());

        final int index = Collections.binarySearch(files, doc, ConfigurationConstants.FILENAME_COMPARATOR);
        if (index >= 0) {
            doc = files.get(index);
        }

        doc.setUuid(graph.getGraphTab());
        doc.setViewport(graph.getViewPortTab());

        if (index < 0) {
            files.add(-index - 1, doc);
        } else {
            files.set(index, doc);
        }
    }

    public void removeOpenedTab(MetanetDiagram graph) {
        final List<DocumentType> files = getSettings().getRestored();

        final URL url;
        if (graph.getSavedFile() != null) {
            try {
                url = graph.getSavedFile().toURI().toURL();
            } catch (MalformedURLException e) {
                return;
            }
        } else {
            return;
        }

        DocumentType doc = (new ObjectFactory()).createDocumentType();
        doc.setUrl(url.toString());

        final int index = Collections.binarySearch(files, doc, ConfigurationConstants.FILENAME_COMPARATOR);
        if (index >= 0) {
            files.remove(index);
        }
    }

    /*
     * Change support methods
     */

    /**
     * Add a PropertyChangeListener for a specific property. The listener will
     * be invoked only when a call on firePropertyChange names that specific
     * property. The same listener object may be added more than once. For each
     * property, the listener will be invoked the number of times it was added
     * for that property. If <code>propertyName</code> or <code>listener</code>
     * is null, no exception is thrown and no action is taken.
     * 
     * @param propertyName
     *            The name of the property to listen on.
     * @param listener
     *            The PropertyChangeListener to be added
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Report a bound property update to any registered listeners. No event is
     * fired if old and new are equal and non-null.
     * 
     * <p>
     * This is merely a convenience wrapper around the more general
     * firePropertyChange method that takes {@code PropertyChangeEvent} value.
     * 
     * @param propertyName
     *            The programmatic name of the property that was changed.
     * @param oldValue
     *            The old value of the property.
     * @param newValue
     *            The new value of the property.
     */
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        changeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }
}
