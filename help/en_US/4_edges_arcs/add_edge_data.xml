<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="add_edge_data">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>add_edge_data</refname>
    <refpurpose>associates new data fields to the edges data structure of a graph </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>g = add_edge_data(g,name [,value])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para>a graph data structure (see <link linkend="graph_data_structure">graph_data_structure</link> )
       </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>name</term>
        <listitem>
          <para>a character string, the name of the data field.
       </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>value</term>
        <listitem>
          <para>a row vector or a matrix with column size equal to the number of edges. This parameter is optional. If it is omitted the data field is set to <literal>[]</literal>.
       </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>g = add_edge_data(g,name [,value])</literal> associates the
     data fields named <literal>name</literal> to the edges data structure
     of the graph <literal>g</literal> and assign it the value given by the
     parameter <literal>value</literal>. If the last argument is not given
     the empty matrix <literal>[]</literal> is assigned.</para>
    <para><literal>value</literal> can be a matrix of any type. The
     <literal>i</literal>th column is associated with the<literal>i</literal>th
     edge. </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
//create a simple graph
ta=[1  1 2 7 8 9 10 10 10 10 11 12 13 13];
he=[2 10 7 8 9 7  7 11 13 13 12 13  9 10];
g=make_graph('simple',1,13,ta,he);
g.nodes.graphics.x=[40,33,29,63,146,233,75,42,114,156,237,260,159];
g.nodes.graphics.y=[7,61,103,142,145,143,43,120,145,18,36,107,107];
show_graph(g,'new')


g=add_edge_data(g,'length',round(10*rand(1,14,'u')));
g=add_edge_data(g,'label','e'+string(1:14));
edgedatafields(g)
g.edges.data.label
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="edgedatafields">edgedatafields</link>
      </member>
      <member>
        <link linkend="edges_data_structure">edges_data_structure</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
