<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="save_graph">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>save_graph</refname>
    <refpurpose>saves a graph in a file</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>save_graph(g,path)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para>a <link linkend="graph_data_structure">graph_data_structure</link>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>path</term>
        <listitem>
          <para>string, the path of the graph to save</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>save_graph</literal> saves the graph <literal>g</literal> in a graph file.
    <literal>path</literal> is the name of the graph file where the graph will be saved.
    <literal>path</literal> can be the name or the pathname of the file; if the 
    <literal>"graph"</literal> extension is missing in <literal>path</literal>, it is assumed.
    If <literal>path</literal> is the name of a directory, the name of the graph is
    used as the name of the file.</para>
    <para> Standard  <link type="scilab" linkend="scilab.help/save">save</link> function may also be used to save a
    graph in a file. In this case take care to save only a single
    graph data structure in the file (without any other variable) if
    you want to reload this file with <link linkend="load_graph">load_graph</link> or
    <link linkend="edit_graph">edit_graph</link>.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
g=load_graph(metanet_module_path()+'/demos/mesh100.graph');
show_graph(g);
save_graph(g,'mymesh100.graph');
g=load_graph('mymesh100.graph');
show_graph(g,'new');
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="load_graph">load_graph</link>
      </member>
      <member>
        <link linkend="edit_graph">edit_graph</link>
      </member>
      <member>
        <link linkend="graph_data_structure">graph_data_structure</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/save">save</link>
      </member>
      <member>
        <link type="scilab" linkend="scilab.help/load">load</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
