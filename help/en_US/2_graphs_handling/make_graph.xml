<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="make_graph">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>make_graph</refname>
    <refpurpose> makes a graph list</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>g = make_graph(name,directed,n,tail,head)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>name</term>
        <listitem>
          <para>string, the name of the graph</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>directed</term>
        <listitem>
          <para>integer, 0 (undirected graph) or 1 (directed graph)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>n</term>
        <listitem>
          <para>integer, the number of nodes of the graph</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>tail</term>
        <listitem>
          <para>row vector of the numbers of the tail nodes of the
          graph (its size is the number of edges of the graph)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>head</term>
        <listitem>
          <para>row vector of the numbers of the head nodes of the
          graph (its size is the number of edges of the graph)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>g</term>
        <listitem>
          <para>a  <link linkend="graph_data_structure">graph_data_structure</link>.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>make_graph</literal> makes a graph list according to its arguments which are
    respectively the name of the graph, a flag for directed or undirected, the
    number of nodes and the row vectors tail and head. These are the minimal data
    needed for a graph.</para>
    <para>If <literal>n</literal> is a positive number, graph <literal>g</literal> has <literal>n</literal> nodes; this
    number must be greater than or equal to <literal>max(max(tail),max(head))</literal>. If
    it is greater than this number,graph <literal>g</literal> has isolated nodes. 
    The nodes names are taken as the nodes numbers.</para>
    <para>If <literal>n</literal> is equal to 0, graph <literal>g</literal> has no isolated node and the number
    of nodes is computed from <literal>tail</literal> and <literal>head</literal>. The nodes names are
    taken from the numbers in <literal>tail</literal> and <literal>head</literal>.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
// creating a directed graph with 3 nodes and 4 arcs.
g=make_graph('foo',1,3,[1,2,3,1],[2,3,1,3]);

// creating a directed graph with 13 nodes and 14 arcs.
ta=[1  1 2 7 8 9 10 10 10 10 11 12 13 13];
he=[2 10 7 8 9 7  7 11 13 13 12 13  9 10];
g=make_graph('foo',1,13,ta,he);
g.nodes.graphics.x=[40,33,29,63,146,233,75,42,114,156,237,260,159]
g.nodes.graphics.y=[7,61,103,142,145,143,43,120,145,18,36,107,107]
show_graph(g)

// creating same graph without isolated node and 14 arcs.
g=make_graph('foo',1,0,ta,he);
g.nodes.graphics.x=[40,33,75,42,114,156,237,260,159];
g.nodes.graphics.y=[7,61,43,120,145,18,36,107,107];
show_graph(g,'new')
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="graph_data_structure">graph_data_structure</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
