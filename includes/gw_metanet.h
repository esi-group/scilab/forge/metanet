/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/* INRIA 2006 */
/* Allan CORNET */

#ifndef __GW_METANET__
#define __GW_METANET__

#include "machine.h"
#include "dynlib_metanet.h"
#include "api_scilab.h"

METANET_IMPEXP int gw_metanet(void);

METANET_IMPEXP int C2F(intsm6loadg)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6saveg)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6prevn2p)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6ns2p)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6p2ns)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6edge2st)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6prevn2st) (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6compc)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6concom)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6compfc)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6sconcom)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6pcchna)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6fordfulk) (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6johns)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6dijkst)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6frang)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6chcm)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6transc)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6dfs)      (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6umtree)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6umtree1)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6dmtree)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6tconex)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6flomax)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6kilter)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6busack)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6floqua)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6relax)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6findiso)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6ta2lpu)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6lp2tad)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6lp2tau)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6dfs2)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6diam)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6cent)     (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6hullcvex) (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6clique)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6clique1)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6hamil)    (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6visitor)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6bmatch)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6knapsk)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6prfmatch) (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6permuto)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6mesh2b)   (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6deumesh)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6bandred)  (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6meshmesh) (char * fname, void * pvApiCtx);
METANET_IMPEXP int C2F(intsm6ford)     (char * fname, void * pvApiCtx);

#endif /*  __GW_METANET__ */

