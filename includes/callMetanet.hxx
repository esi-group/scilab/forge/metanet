/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __CALLMETANET_HXX__
#define __CALLMETANET_HXX__
/*--------------------------------------------------------------------------*/
/*
 * Header that declare all wrapper to CXX functions
 * @see Metanet.hxx
 */
extern "C" {

int callMetanet(char const * variable, char const * file,
                      double window,         double scale,
              double const * winsize,           int winsizeSize);

double callNetwindow(double index);

double* callNetwindows(int * len);

void callMetanetShow(bool nodes,                 bool edges,
           double const * indexes,                int indexesSize,
                     bool clearPrevious, char const * message);

void callWarnObjectByUID(char const * UID, char const * message);

void closeMetanetFromScilab();

void loadMetanetFile(char const * filename);

}

#endif /* !__CALLMETANET_HXX__ */
