/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2012-2012 - Scilab Enteprises - Clement David
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "callMetanet.hxx"

extern "C"
{
#include "api_scilab.h"
#include "sci_malloc.h"
#include "Scierror.h"
#include "localization.h"
// export a C symbol
int sci_metanet_load(char * fname, void * pvApiCtx);
}

int sci_metanet_load(char * fname, void * pvApiCtx)
{
    CheckRhs(1, 1);
    CheckLhs(0, 1);
    SciErr sciErr;

    // shared address
    int * piAddressFilename;

    char * filename;

    /* getting filename */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressFilename);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (getAllocatedSingleString(pvApiCtx, piAddressFilename, &filename))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: "
                        "A string expected.\n"),
                 fname, 1);
        return 0;
    }

    /* call the implementation : C (ABI) -> C++ (JNI) -> Java */
    loadMetanetFile(filename);
    FREE(filename);

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}

