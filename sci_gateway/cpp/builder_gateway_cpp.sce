//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
// Copyright (C) 2010-2012 - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function builder_gw_cpp()

    CURRENT_PATH = strsubst(
            get_absolute_file_path("builder_gateway_cpp.sce"), "\", "/");
    LD_FLAGS = [];

    INCLUDES_PATHS = "-I" + CURRENT_PATH + "../../includes/";

    if getos() == 'Windows' then
        INCLUDES_PATHS = INCLUDES_PATHS ..
                       + " -I" + SCI + "/modules/fileio/includes/";
        LD_FLAGS = SCI+"/bin/fileio.lib";
    else
        // Source tree version
        if isdir(SCI+"/modules/core/includes/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I" + SCI + "/modules/fileio/includes/";
        end

        // Binary version
        if isdir(SCI+"/../../include/scilab/core/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I" + SCI + "/../../include/scilab/fileio/";
        end

        // System version (ie: /usr/include/scilab/)
        if isdir("/usr/include/scilab/") then
            INCLUDES_PATHS = INCLUDES_PATHS ..
                           + " -I/usr/include/scilab/fileio/";
        end
    end

    LIBS_PATHS = ['../../src/jni/libmetanet_jni'];
    FUNCTIONS_GATEWAY = [
            'metanet_edit_graph'     'sci_metanet_edit_graph'
            'warnObjectByUID'        'sci_warn_object_by_uid'
            'closeMetanetFromScilab' 'sci_close_metanet_from_scilab'
            'netwindow'              'sci_netwindow'
            'netwindows'             'sci_netwindows'
            'metanet_show'           'sci_metanet_show'
            'metanet_load'           'sci_metanet_load'
            'm6loadg'      'sci_m6loadg'
            'm6saveg'      'sci_m6saveg'
            'm6bandred'    'sci_m6bandred'];
    FILES_GATEWAY = [
            'sci_metanet_edit_graph.cpp'
            'sci_warn_object_by_uid.cpp'
            'sci_close_metanet_from_scilab.cpp'
            'sci_netwindow.cpp'
            'sci_metanet_show.cpp'
            'sci_metanet_load.cpp'
            'sci_m6loadg.cpp'
            'sci_m6saveg.cpp'
            'sci_m6bandred.cpp'
            ];

    WITHOUT_AUTO_PUTLHSVAR = %t;

    tbx_build_gateway(
            'gw_metanet_cpp',
            FUNCTIONS_GATEWAY,
            FILES_GATEWAY,
            CURRENT_PATH,
            LIBS_PATHS,
            LD_FLAGS,
            INCLUDES_PATHS);
endfunction

builder_gw_cpp();
clear builder_gw_cpp;

