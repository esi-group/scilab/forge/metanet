/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 */

#include "function.hxx"
#include "double.hxx"

extern "C"
{
#include "Scierror.h"
#include "localization.h"
void C2F(bandred)(
        int * n,      int * nz,
        int * liwork, int * iwork,
        int * lrwork, int * rwork,
        int * optpro, int * ierr,
        int * iperm,  int * mrepi,
        int * iband);
}

types::Function::ReturnValue
sci_m6bandred(
        typed_list & in,
        int iRetCount,
        typed_list & out)
{

    types::Double * n      = NULL;
    types::Double * nz     = NULL;
    types::Double * liwork = NULL;
    types::Double * iwork  = NULL;
    types::Double * lrwork = NULL;
    types::Double * rwork  = NULL;
    types::Double * optpro = NULL;
    types::Double * iperm  = NULL;
    types::Double * mrepi  = NULL;
    types::Double * iband  = NULL;
    types::Double * ierr   = NULL;

    int n_i    = 0;
    int ierr_i = 0;

    char * fname = "m6bandred";

    if (in.size() != 7)
    {
        Scierror(999, _("%s: Invalid number of input arguments: "
                        "Expected %d, got %d instead.\n"),
                 fname, 7, in.size());
        return 1;
    }

    if (iRetCount < 1
     || iRetCount > 4)
    {
        Scierror(999, _("%s: Invalid number of output arguments: "
                        "Expected from %d to %d, got %d instead.\n"),
                 fname, 1, 4, iRetCount);
        return 1;
    }

    /* checking variable n (number 1) */
    if (in[0]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a scalar double for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "n", 1, in[0]->getTypeStr());
        return 1;
    }
    n = in[0]->getAs<types::Double>();
    if (n->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a scalar for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "n", 1, n->getRows(), n->getCols());
        return 1;
    }
    n->convertToInteger();
    /* get value of n */
    n_i = *((int*)(n->get()));

    /* checking variable nz (number 2) */
    if (in[1]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a scalar double for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "nz", 2, in[1]->getTypeStr());
        return 1;
    }
    nz = in[1]->getAs<types::Double>();
    if (nz->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a scalar for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "nz", 2, nz->getRows(), nz->getCols());
        return 1;
    }
    nz->convertToInteger();

    /* checking variable liwork (number 3) */
    if (in[2]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a scalar double for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "liwork", 3, in[2]->getTypeStr());
        return 1;
    }
    liwork = in[2]->getAs<types::Double>();
    if (liwork->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a scalar for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "liwork", 3,
                 liwork->getRows(), liwork->getCols());
        return 1;
    }
    liwork->convertToInteger();

    /* checking variable iwork (number 4) */
    if (in[3]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a double vector for "
                        "input argument %s at position #%d, "
                        "but got %s instead.\n"),
                 fname, "iwork", 4, in[3]->getTypeStr());
        return 1;
    }
    iwork = in[3]->getAs<types::Double>();
    if (iwork->isVector() == false)
    {
        Scierror(999, _("%s: Expected a vector for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "iwork", 4,
                 iwork->getRows(), iwork->getCols());
        return 1;
    }
    iwork->convertToInteger();

    /* checking variable lrwork (number 5) */
    if (in[4]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a scalar double for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "lrwork", 5, in[4]->getTypeStr());
        return 1;
    }
    lrwork = in[4]->getAs<types::Double>();
    if (lrwork->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a scalar for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "lrwork", 5,
                 lrwork->getRows(), lrwork->getCols());
        return 1;
    }
    lrwork->convertToInteger();

    /* checking variable rwork (number 6) */
    if (in[5]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a double vector for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "rwork", 6, in[5]->getTypeStr());
        return 1;
    }
    rwork = in[5]->getAs<types::Double>();
    if (rwork->isVector() == false)
    {
        Scierror(999, _("%s: Expected a vector for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "rwork", 6,
                 rwork->getRows(), rwork->getCols());
        return 1;
    }

    /* checking variable optpro (number 7) */
    if (in[6]->isDouble() == false)
    {
        Scierror(999, _("%s: Expected a scalar double for "
                        "input argument %s at position #%d, "
                        "but got a %s instead.\n"),
                 fname, "optpro", 7, in[6]->getTypeStr());
        return 1;
    }
    optpro = in[6]->getAs<types::Double>();
    if (optpro->isScalar() == false)
    {
        Scierror(999, _("%s: Expected a scalar for "
                        "input argument %s at position #%d, "
                        "but got a (%d, %d) instead.\n"),
                 fname, "optpro", 7,
                 optpro->getRows(), optpro->getCols());
        return 1;
    }
    optpro->convertToInteger();

    /* no cross variable size checking ?! */

    /* prepare output variables */
    iperm = new types::Double(n_i, 1);
    mrepi = new types::Double(n_i, 1);
    iband = new types::Double(n_i, 1);
    ierr  = new types::Double(1, 1);
    iperm->setViewAsInteger(true);
    mrepi->setViewAsInteger(true);
    iband->setViewAsInteger(true);
    ierr ->setViewAsInteger(true);

    /*************************/
    /*                       */
    /*    CALL TO BANDRED    */
    /*                       */
    /*************************/
    C2F(bandred)(
            /* input */
            (int*)(n->get()),
            (int*)(nz->get()),
            (int*)(liwork->get()),
            (int*)(iwork->()),
            (int*)(lrwork->get()),
            rwork->get(),
            (int*)(optpro->get()),
            /* output */
            (int*)(ierr->get()),
            (int*)(iperm->get()),
            (int*)(mrepi->get()),
            (int*)(iband->get()));

    /* check error */
    ierr_i = *((int*)(ierr->get()));
    if (ierr_i)
    {
        /* bandred should display its error messages */
        delete iperm;
        delete mrepi;
        delete iband;
        delete ierr;
        return 1;
    }

    /* return output variables as doubles */
    out.reserve(iRetCount);
    switch (iRetCount)
    {
    case 4 : out[3] = ierr->convertFromInteger();
    case 3 : out[2] = iband->convertFromInteger();
    case 2 : out[1] = mrepi->convertFromInteger();
    }
    out[0] = iperm->convertFromInteger();

    /* free unused variables */
    switch (iRetCount)
    {
    case 1 : delete mrepi;
    case 2 : delete iband;
    case 3 : delete ierr;
    }

    return 0;

}
