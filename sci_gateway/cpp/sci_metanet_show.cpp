/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2012-2012 - Scilab Enteprises - Clement David
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "callMetanet.hxx"

extern "C"
{
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
// export a C symbol
int sci_metanet_show(char * fname, void * pvApiCtx);
}

int sci_metanet_show(char * fname, void * pvApiCtx)
{
    CheckRhs(5, 5);
    CheckLhs(0, 1);
    SciErr sciErr;

    // shared address
    int * piAddress = NULL;
    int argCounter = 0;

    int nodes;
    int edges;

    double * indexes;
    int indexes_m;
    int indexes_n;

    int clearPrevious;
    char * message;

    /* getting nodes */
    argCounter++;
    sciErr = getVarAddressFromPosition(pvApiCtx, argCounter, &piAddress);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (getScalarBoolean(pvApiCtx, piAddress, &nodes))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: "
                        "A boolean expected.\n"),
                 fname, argCounter);
        return 0;
    }

    /* getting edges */
    argCounter++;
    sciErr = getVarAddressFromPosition(pvApiCtx, argCounter, &piAddress);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (getScalarBoolean(pvApiCtx, piAddress, &edges))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: "
                        "A boolean expected.\n"),\
                 fname, argCounter);
        return 0;
    }

    /* getting indexes */
    argCounter++;
    sciErr = getVarAddressFromPosition(pvApiCtx, argCounter, &piAddress);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getMatrixOfDouble(
            pvApiCtx, piAddress,
            &indexes_m, &indexes_n, &indexes);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    /* getting clearPrevious */
    argCounter++;
    sciErr = getVarAddressFromPosition(pvApiCtx, argCounter, &piAddress);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (getScalarBoolean(pvApiCtx, piAddress, &clearPrevious))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: "
                        "A boolean expected.\n"),
                 fname, argCounter);
        return 0;
    }

    /* getting message */
    argCounter++;
    sciErr = getVarAddressFromPosition(pvApiCtx, argCounter, &piAddress);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (getAllocatedSingleString(pvApiCtx, piAddress, &message))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: "
                        "A string expected.\n"),
                 fname, argCounter);
        return 0;
    }

    /* call the implementation : C (ABI) -> C++ (JNI) -> Java */
    callMetanetShow(
            nodes, edges, indexes,
            indexes_m * indexes_n, clearPrevious, message);

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}

