/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2012-2012 - Scilab Enteprises - Clement David
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "callMetanet.hxx"
/*--------------------------------------------------------------------------*/
extern "C"
{
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"

// export a C symbol
int sci_netwindow(char *fname,unsigned long fname_len);
int sci_netwindows(char *fname,unsigned long fname_len);
}

/*--------------------------------------------------------------------------*/
int sci_netwindow(char *fname,unsigned long fname_len)
{
	CheckRhs(1,1);
	CheckLhs(0,1);
	SciErr err;

	int *piAddressVarOne = NULL;
	double index;

	err = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	if(getScalarDouble(pvApiCtx, piAddressVarOne, &index))
	{
		Scierror(999,_("%s: Wrong type for input argument #%d: A double expected.\n"),fname,1);
		return 0;
	}

	callNetwindow(index);

	LhsVar(1) = 0; PutLhsVar();
	return 0;
}
/* ==================================================================== */

int sci_netwindows(char *fname,unsigned long fname_len)
{
	CheckRhs(0,0);
	CheckLhs(1,1);
	SciErr err;
	int i;

	double current;

	double* netwindows;
	int netwindows_len;

	int* addr;

	// do not update (-1) and retrieve the current window
	current = callNetwindow(-1);

	// retrieve window list
	netwindows = callNetwindows(&netwindows_len);

	err = createList(pvApiCtx, Rhs + 1, 2, &addr);
	if(err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	err = createMatrixOfDoubleInList(pvApiCtx, Rhs + 1, addr, 1, 1, netwindows_len, netwindows);
	if(err.iErr)
	{
		printError(&err, 0);
		return 0;
	}
	err = createMatrixOfDoubleInList(pvApiCtx, Rhs + 1, addr, 2, 1, 1, &current);
	if(err.iErr)
	{
		printError(&err, 0);
		return 0;
	}

	LhsVar(1) = Rhs + 1;
	PutLhsVar();
	return 0;
}
/* ==================================================================== */
