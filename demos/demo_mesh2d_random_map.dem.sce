// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function demo_mesh2d_random_map()
    // Built from mesh2d() examples: Case #4

    // DEMO START
    f = scf(100001);
    clf()
    f.figure_size = [700 700];
    f.figure_name = "METANET: mesh2d() meshing a random set of points";
    demo_viewCode("demo_mesh2d_random_map.dem.sce");

    N = 500;
    X = rand(1, N);
    Y = rand(1, N);

    Tr = mesh2d(X,Y);

    plot2d(X, Y, [-1 -2 3]);
    [m, n] = size(Tr);
    f.color_map = rand(2*n, 3);
    xpols = matrix(X(Tr), m, n);
    ypols = matrix(Y(Tr), m, n);
    xfpolys(xpols, ypols, [n/4:n/4+n-1]);

    // ------------------------------------------------------------------------
    // DEMO END

endfunction

demo_mesh2d_random_map();
clear demo_mesh2d_random_map;














