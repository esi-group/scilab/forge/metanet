// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Samuel GOUGEON
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

demopath = get_absolute_file_path("metanet.dem.gateway.sce");

subdemolist = [                                   ..
    "Meshing random points" "demo_mesh2d_random_map.dem.sce"
    "mesh2d: free mesh"     "demo_mesh2d_1.dem.sce"
    "mesh2d: regular mesh"  "demo_mesh2d_regular.dem.sce"
    "Metro in Paris"        "demo_metro.dem.sce"];

subdemolist(:, 2) = demopath + subdemolist(:, 2);
clear demopath;
