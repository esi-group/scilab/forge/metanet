// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
// Copyright (C) ????-2008 - INRIA - Claude GOMEZ <claude.gomez@inria.fr>
// Copyright (C) 2012 - DIGITEO - Allan CORNET
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- TEST WITH GRAPHIC -->

// unit tests for mat_2_graph function
// =============================================================================
g = load_graph(metanet_module_path() + '/demos/colored.graph');
show_graph(g);
a = graph_2_mat(g);
g1 = mat_2_graph(a,1);
g1('node_x') = g('node_x');
g1('node_y') = g('node_y');
show_graph(g1, 'new');
a = graph_2_mat(g, 'node-node');
g1 = mat_2_graph(a, 1, 'node-node');
g1('node_x') = g('node_x');
g1('node_y') = g('node_y');
show_graph(g1, 'new');
