// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Clement David
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- Non-regression test for issue 389 -->
//
// <-- ENGLISH IMPOSED -->
//
// <-- Bugzilla URL -->
// http://forge.scilab.org/index.php/p/metanet/issues/389/
//
// <-- Short Description -->
// When applying cycle_basis on a graph without loop, cycle_basis abort and
// prints an error. A warning with an empty matrix of cycles should a lot more
// better.

g = [];

// Node parameters
node_x           = [250 150 350 100 200 300 400];
node_y           = [300 200 200 100 100 100 100];
node_z           = [0   10  10  20  20  20  20];
consumption_node = [10 8 8 1 2 3 4];

// Edge parameters
I_start   = [1 1 2 2 3 3];
I_end     = [2 3 4 5 6 7];
diameter  = [50 50 25 25 25 25];
flow_pipe = [0 0 0 0 0 0];

NodeNum = 7;
InstanceName = 'simple';
I_head = [1];
I_tail = [4 5 6 7];

g = make_graph(InstanceName,1,NodeNum,I_start,I_end);
// Node parameters
g('node_x') = node_x;
g('node_y') = node_y;

g('head') = I_head;
g('tail') = I_tail;
g('default_node_diam') = (max(g('node_x') - min(g('node_x')) + max(g('node_y') - min(g('node_y')))))/500;

cycles_list = cycle_basis(graph_simp(g));
