
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA - Serge STEER <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ok, GraphList] = ge_do_add_edge_data(GraphList)

    desc = ["Add a  new edge data field"
            "  enter the field name"
            "  and the default value for this field"];
    def = ["" ""];

    data = GraphList.edges.data;
    F = getfield(1, data);
    while %t
        res = x_mdialog(desc, ["Name" "Default Value"], def);
        if res == [] then
            return;
        end
        name = stripblanks(res(1));
        if execstr(name + " = 1", "errcatch") <> 0 then
            messagebox("Invalid Field name", "modal", "error");
            continue;
        end
        if or(F(2:$) == name) then
            messagebox("This field name is already defined", "modal", "error");
            continue;
        end
        ierr = execstr("default = " + res(2), "errcatch");
        if ierr <> 0 then
            messagebox("Answer for ""Default Value"" " ..
                     + "cannot be evaluated", "modal", "error");
            continue;
        end
        break;
    end
    n = size(GraphList.edges);
    F(1, $+1) = name;
    setfield(1, F, data);
    if size(default,2) > 1 then
        d = list();
        for k = 1:n
            d(k) = default;
        end
        data(name) = d;
    else
        if n > 0 then
            data(name) = default(:, ones(1, n));
        else
            data(name) = [];
        end
    end
    GraphList.edges.data = data;
endfunction
