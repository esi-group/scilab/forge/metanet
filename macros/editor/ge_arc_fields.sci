
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2002-2008 - INRIA - Serge STEER <serge.steer@inria.fr>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function fn = ge_arc_fields()
    fn = ["tail"
          "head"
          "edge_name"
          "edge_color"
          "edge_width"
          "edge_hi_width"
          "edge_font_size"
          "edge_length"
          "edge_cost"
          "edge_min_cap"
          "edge_max_cap"
          "edge_q_weight"
          "edge_q_orig"
          "edge_weight"
          "edge_label"]';
endfunction
